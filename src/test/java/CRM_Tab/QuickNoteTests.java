package CRM_Tab;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.quickNoteTests;

public class QuickNoteTests extends quickNoteTests {

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}

	@Test
	public void a_Click_QuickNotesFor_Subscriber() throws InterruptedException
	{
		ClickQuickNotes();	
	}
	
	@Test
	public void b_Click_Create_Quicknote() throws InterruptedException
	{
		
		ClickCreateQuickNote();
		Thread.sleep(1000);
	}
	
	@Test
	public void c_Select_type() throws InterruptedException
	{
		Thread.sleep(1000);
		Select_Type();
	}
	
	@Test
	public void d_Enter_Subject()
	{
		Enter_subject();
	}
	
	@Test
	public void e_Enter_Note()
	{
		
		Enter_note();
	}
	
	@Test
	public void f_Select_Status()
	{
		Select_status();
	}
	
	@Test
	public void g_Select_FaultType()
	{
		Select_faultType();
	}
	
	@Test
	public void h_Click_Create()
	{
		Click_create();
	}
	
	@Test
	public void i_check_success_message()
	{
		suceess_msg();
	}
	
	@Test
	public void la_VerifyCreatedNote() throws InterruptedException 
	{
		Thread.sleep(1000);
		Verify_note();
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to click quick notes **********************************
public void ClickQuickNotes() throws InterruptedException
{
	try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
	
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Boolean name= QuickNotes.isDisplayed();
		
		if(name==true)  {
		  
			wait.equals(name);
		    
			Actions act= new Actions(driver);
		    
			Thread.sleep(1500);
			
			act.moveToElement(QuickNotes).click().build().perform();
			
			Thread.sleep(1500);
		    
		}
		
	}catch(StaleElementReferenceException e)    {
	
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Actions act= new Actions(driver);
		
		Thread.sleep(1500);
		
		act.moveToElement(QuickNotes).click().build().perform();
		
		Thread.sleep(1500);
		}
	}


//********************************** Method to Click create Quick notes********************************
public void ClickCreateQuickNote()
{
	try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
	
		WebElement create= driver.findElement(By.linkText("Create"));
		
		Boolean name= create.isDisplayed();
		
		if(name==true)   {
		  
			wait.equals(name);
		    
			Actions act= new Actions(driver);
			
			act.moveToElement(create).click().build().perform();
			
			act.moveToElement(create).click().build().perform();

		    }
		
	}catch(StaleElementReferenceException e) {
			
		WebElement create= driver.findElement(By.linkText("Create"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(create).click().build().perform();
		
		act.moveToElement(create).click().build().perform();
	}
}
		
//*************************************  Method to select type *************************************
public void Select_Type()
	{

	Select dropdown= new Select(driver.findElement(By.id("P25_QN_QNT_UID")));
	
	dropdown.selectByVisibleText("ACS");
			
}
		
//*************************************  Method to enter subject *************************************	
public void Enter_subject()
{
	driver.findElement(By.id("P25_QN_SUBJECT")).sendKeys("new subject");

}
		
//*************************************  Method to enter note *************************************	
public void Enter_note()
{
	driver.findElement(By.id("P25_DETAIL")).sendKeys("test note data");
			
}
		
//*************************************  Method to select status *************************************
public void Select_status()
{
	driver.findElement(By.id("P25_QN_SS_UID_5")).click();

}

//*************************************  Method to fault type *************************************
public void Select_faultType()
{
	Select dropdown= new Select(driver.findElement(By.id("P25_QN_FAULT_QNT_UID")));

	dropdown.selectByVisibleText("Write Off");
	
}
		
//*************************************  Method to clickCreate note *************************************
public void Click_create()
{
	driver.findElement(By.id("B13529453749207036")).click();

}

//********************************* Method to check success message ***************************
public void suceess_msg()
{
	WebElement msg= driver.findElement(By.id("echo-message"));
	
	String MSG= msg.getText();
	
	String Exp_Msg= "Action Processed.";
	
	Assert.assertEquals(MSG, Exp_Msg);
	
}

//************************************* Method to verify created quick note ************************
 public void Verify_note()
 {
	 WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[10]"));

	 String DATE= datestamp.getText();
	
	 System.out.println(DATE);
	
	 DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	 Date date = new Date();
	
	 String date1= dateFormat.format(date);
	
	 System.out.println(date1);
	
	 String Date2= DATE.substring(0, 17);
	
	 Assert.assertEquals(Date2, date1);
	
 }
}
